<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:android='http://schemas.android.com/apk/res/android'>
 	<xsl:param name="DEFAULTIMAGE.SRC"></xsl:param>
	<xsl:output indent="no" />

	<!-- Supplies a newline after the XML header. It's cosmetic, but something 
		to include in each script. -->
	<xsl:template match="/">
		<xsl:text>
		</xsl:text>
		<xsl:apply-templates select="@*|node()" />
	</xsl:template>
 
	<xsl:template match="ImageView">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<xsl:attribute name="android:src"><xsl:value-of
				select="$DEFAULTIMAGE.SRC" /></xsl:attribute>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="LinearLayout">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<xsl:attribute name="android:src"><xsl:value-of
				select="$DEFAULTIMAGE.SRC" /></xsl:attribute>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
