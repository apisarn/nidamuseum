package com.bomzaiya.museumpost;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.WindowManager;

/**
 * This is just used in order to be able to recognize home button presses
 * 
 * @author Apisarn
 * 
 */
@SuppressLint("WorldReadableFiles")
public class BomPrefActivity extends PreferenceActivity {

  protected ProductConfig mProductConfig;
  protected BomApplication mApplication;
  protected ProgressDialog mProgressDialog = null;

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {

  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mApplication = (BomApplication) getApplication();

    configurePreference();
  }

  @Override
  protected void onResume() {
    super.onResume();

  }

  @Override
  public void onStop() {
    super.onStop();

  }

  @SuppressWarnings("deprecation")
  private void configurePreference() {
    getPreferenceManager().setSharedPreferencesName(BomApplication.PREFERENCES);
    getPreferenceManager().setSharedPreferencesMode(
        BomApplication.PREFERENCE_MODE);
  }

}
