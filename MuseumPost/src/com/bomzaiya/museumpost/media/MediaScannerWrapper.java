package com.bomzaiya.museumpost.media;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;

public class MediaScannerWrapper implements
    MediaScannerConnection.MediaScannerConnectionClient {

  public interface OnScanMedia {
    public abstract void onScanMediaComplete();
  }

  private MediaScannerConnection mConnection;
  private String mPath;
  private String mMimeType;

  private OnScanMedia mOnScanMedia;

  // filePath - where to scan;
  // mime type of media to scan i.e. "image/jpeg".
  // use "*/*" for any media
  public MediaScannerWrapper(Context ctx, String filePath, String mime) {
    mPath = filePath;
    mMimeType = mime;
    mConnection = new MediaScannerConnection(ctx, this);
  }

  public MediaScannerWrapper(Context ctx, String filePath, String mime,
      OnScanMedia onScanMedia) {
    mOnScanMedia = onScanMedia;
    mPath = filePath;
    mMimeType = mime;
    mConnection = new MediaScannerConnection(ctx, this);
  }

  // do the scanning
  public void scan() {
    mConnection.connect();
  }

  // start the scan when scanner is ready
  public void onMediaScannerConnected() {
    mConnection.scanFile(mPath, mMimeType);
    Log.w("MediaScannerWrapper", "media file scanned: " + mPath);
  }

  public void onScanCompleted(String path, Uri uri) {
    mConnection.disconnect();
    if (mOnScanMedia != null) {
      mOnScanMedia.onScanMediaComplete();
    }
  }

}