package com.bomzaiya.museumpost;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PostResultActivity extends BomActivity {
  protected static final int POST_RESULT = 1777;
  protected static final int CHOOSE_FRAME = 100;
  private static final String RESULT = "result";
  private static final String MSG = "msg";

  public static final int FAIL = 0;
  public static final int SUCCESS = 1;
  private int mResult;
  private String mMsg;

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }

  public static void startForResult(BomActivity activity, int result, String msg) {
    Intent intent = new Intent(activity, PostResultActivity.class);
    intent.putExtra(RESULT, result);
    intent.putExtra(MSG, msg);
    activity.startActivityForResult(intent, POST_RESULT);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    // outState.put
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.post_result);

    Intent intent = getIntent();
    mResult = intent.getIntExtra(RESULT, FAIL);
    mMsg = intent.getStringExtra(MSG);

    TextView tvResult = (TextView) findViewById(R.id.tvResult);

    String message = "";
    if (mResult == SUCCESS) {
      message = getString(R.string.post_success) + "\r" + mMsg;
    } else {
      message = getString(R.string.post_fail) + "\r" + mMsg;
    }
    tvResult.setText(message);

    // Add a listener to the Capture button
    Button buttonBack = (Button) findViewById(R.id.buttonBack);
    buttonBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mUIHandler.removeMessages(HANDLER_SHOW_RESULT);
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
      }
    });

    Button buttonChooseFrame = (Button) findViewById(R.id.buttonChooseFrame);
    buttonChooseFrame.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mUIHandler.removeMessages(HANDLER_SHOW_RESULT);
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        intent.putExtra(BACK_TO, ChooseFrameActivity.class.getName());
        finish();
      }
    });

    mUIHandler.removeMessages(HANDLER_SHOW_RESULT);
    Message msg = mUIHandler.obtainMessage(HANDLER_SHOW_RESULT);
    mUIHandler.sendMessageDelayed(msg, SHOW_RESULT);

    ImageView ivQRcode = (ImageView) findViewById(R.id.ivQRcode);
    if (mResult == FAIL) {
      ivQRcode.setVisibility(ImageView.GONE);
    }
  }

  private MyHandler mUIHandler = new MyHandler(this);

  protected static final int HANDLER_SHOW_RESULT = 1;

  protected static final int SHOW_RESULT = 20000; // 20 secs.

  private static class MyHandler extends Handler {
    private WeakReference<PostResultActivity> mActivity = null;

    public MyHandler(PostResultActivity activity) {
      mActivity = new WeakReference<PostResultActivity>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
      final PostResultActivity activity = mActivity.get();
      if (activity != null) {

        switch (msg.what) {
        case HANDLER_SHOW_RESULT:
          Intent intent = new Intent();
          activity.setResult(Activity.RESULT_OK, intent);
          intent.putExtra(BACK_TO, PreloadActivity.class.getName());
          activity.finish();
          break;
        }
      }
    }
  }

}
