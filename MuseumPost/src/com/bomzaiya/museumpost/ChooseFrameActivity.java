package com.bomzaiya.museumpost;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ChooseFrameActivity extends BomActivity {
  protected static final int CHOOSE_FRAME = 100;

  public static void startForResult(BomActivity activity) {
    Intent intent = new Intent(activity, ChooseFrameActivity.class);
    activity.startActivityForResult(intent, CHOOSE_FRAME);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
      case LiveCameraActivity.LIVE_CAMERA:
        String backTo = data.getStringExtra(BACK_TO);
        if (!getClass().getName().equals(backTo)) {
          setResult(Activity.RESULT_OK, data);
          finish();
        }
        break;

      default:
        break;
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    // outState.put
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.choose_frame);

    Button ivFrame1 = (Button) findViewById(R.id.ivFrame1);
    Button ivFrame2 = (Button) findViewById(R.id.ivFrame2);
    Button ivFrame3 = (Button) findViewById(R.id.ivFrame3);
    Button ivFrame4 = (Button) findViewById(R.id.ivFrame4);

    ivFrame1.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        saveFrameUse(1);
        LiveCameraActivity.startForResult(ChooseFrameActivity.this,
            "frame_1.png");
      }
    });

    ivFrame2.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        saveFrameUse(2);
        LiveCameraActivity.startForResult(ChooseFrameActivity.this,
            "frame_2.png");
      }
    });

    ivFrame3.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        saveFrameUse(3);
        LiveCameraActivity.startForResult(ChooseFrameActivity.this,
            "frame_3.png");
      }
    });

    ivFrame4.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        saveFrameUse(4);
        LiveCameraActivity.startForResult(ChooseFrameActivity.this,
            "frame_4.png");
      }
    });

    Button buttonBack = (Button) findViewById(R.id.buttonBack);
    buttonBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
      }
    });
  }

  private void saveFrameUse(int frame) {
    // frame
    // date, frame no, use++
    StatDatabaseAdapter db = new StatDatabaseAdapter(mApplication);
    db.updateFrameRecord(frame);
    db.close();
  }
}
