package com.bomzaiya.museumpost;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bomzaiya.museumpost.media.MediaScannerWrapper;

public class LiveCameraActivity extends BomActivity {

  private static final String INTENT_TAG_FRAME = "frame_name";
  protected static final String TAG = "LIVECAMERA";
  public static final int LIVE_CAMERA = 111;

  public static final int ORIENTATION_LANDSCAPE_LEFT = 1;
  public static final int ORIENTATION_LANDSCAPE_RIGHT = 2;
  public static final int ORIENTATION_PORTRAIT_UP = 3;
  public static final int ORIENTATION_PORTRAIT_DOWN = 4;
  private int mOrientation;
  private SensorManager mSensorManager;
  private SensorEventListener mSensorListener;
  private static Camera mCamera;
  private CameraPreview mPreview;
  private String mFrameName;
  private FrameLayout mLayoutCameraFrame;
  private File mBackgroundFile;
  private Bitmap mBmFrame;
  private FrameLayout mCameraPreview;
  private TextView mTvCounter;

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
      case PhotoPreviewActivity.PHOTO_PREVIEW:
        String backTo = data.getStringExtra(BACK_TO);
        if (!getClass().getName().equals(backTo)) {
          setResult(Activity.RESULT_OK, data);
          finish();
        }
        break;
      default:
        break;
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  public int getCurrentOrientation() {
    return mOrientation;
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    // outState.put
    super.onSaveInstanceState(outState);
  }

  public static void startForResult(BomActivity activity) {
    Intent intent = new Intent(activity, LiveCameraActivity.class);
    activity.startActivityForResult(intent, LIVE_CAMERA);
  }

  public static void startForResult(BomActivity activity, String frame) {
    Intent intent = new Intent(activity, LiveCameraActivity.class);
    intent.putExtra(INTENT_TAG_FRAME, frame);
    activity.startActivityForResult(intent, LIVE_CAMERA);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.camera);

    Intent intent = getIntent();
    mFrameName = intent.getStringExtra(INTENT_TAG_FRAME);
  }

  private Drawable getBackgroundDrawable() {
    try {
      mBackgroundFile = new File(InstructionActivity.FRAME_FOLDER + "/", mFrameName);

      InputStream is = new FileInputStream(mBackgroundFile);
      mBmFrame = BitmapFactory.decodeStream(is, null, null);

      Drawable drawable = null;
      drawable = new BitmapDrawable(getResources(), mBmFrame);
      return drawable;
    } catch (IOException e) {
    }
    return null;
  }

  @SuppressWarnings("deprecation")
  protected void setDisplayOrientation(Camera camera, int angle, int mode) {
    Parameters parameters = camera.getParameters();
    if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
      Method downPolymorphic;
      try {
        downPolymorphic = camera.getClass().getMethod("setDisplayOrientation",
            new Class[] { int.class });
        if (downPolymorphic != null)
          downPolymorphic.invoke(camera, new Object[] { angle });
      } catch (Exception e1) {
      }
    } else {
      switch (mode) {
      case ORIENTATION_PORTRAIT_UP:
      case ORIENTATION_PORTRAIT_DOWN:
        parameters.set("orientation", "portrait");
        parameters.set("rotation", angle);
        break;
      case ORIENTATION_LANDSCAPE_LEFT:
      case ORIENTATION_LANDSCAPE_RIGHT:
        parameters.set("orientation", "landscape");
        parameters.set("rotation", angle);
        break;

      default:
        break;
      }

    }

  }

  private void registerSensor() {
    mSensorManager = (SensorManager) this
        .getSystemService(Context.SENSOR_SERVICE);
    mSensorListener = new SensorEventListener() {

      @Override
      public void onSensorChanged(SensorEvent event) {
        float eventVal = event.values[1];

        if (eventVal >= 6.5) {
          if (mOrientation != ORIENTATION_PORTRAIT_UP) {
            // onOrientationChanged(ORIENTATION_PORTRAIT_UP);
          }
          mOrientation = ORIENTATION_PORTRAIT_UP;
        } else if (-6.5 < eventVal && eventVal < 6.5) {
          if (event.values[0] > 0) {
            if (mOrientation != ORIENTATION_LANDSCAPE_LEFT) {
              // onOrientationChanged(ORIENTATION_LANDSCAPE_LEFT);
            }
            mOrientation = ORIENTATION_LANDSCAPE_LEFT;
          } else {
            if (mOrientation != ORIENTATION_LANDSCAPE_RIGHT) {
              // onOrientationChanged(ORIENTATION_LANDSCAPE_RIGHT);
            }
            mOrientation = ORIENTATION_LANDSCAPE_RIGHT;
          }
        } else {
          if (mOrientation != ORIENTATION_PORTRAIT_DOWN) {
            // onOrientationChanged(ORIENTATION_PORTRAIT_DOWN);
          }
          mOrientation = ORIENTATION_PORTRAIT_DOWN;
        }

      }

      @Override
      public void onAccuracyChanged(Sensor sensor, int accuracy) {

      }
    };
    mSensorManager.registerListener(mSensorListener,
        mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
        SensorManager.SENSOR_DELAY_GAME);

  }

  private MyHandler mUIHandler = new MyHandler(this);

  protected static final int HANDLER_COUNTER = 1;

  protected static final int COUNTER_INTERVAL = 900; // 1 sec
  protected static final int COUNTER_COUNT = 5; // 1 sec

  private int mCounter = 0;
  private FrameLayout mLayoutCounter;

  private static class MyHandler extends Handler {
    private WeakReference<LiveCameraActivity> mActivity = null;

    public MyHandler(LiveCameraActivity activity) {
      mActivity = new WeakReference<LiveCameraActivity>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
      final LiveCameraActivity activity = mActivity.get();
      if (activity != null) {

        switch (msg.what) {
        case HANDLER_COUNTER:
          activity.mCounter++;
          if (activity.mCounter > COUNTER_COUNT) {
            // get an image from the camera
            activity.mUIHandler.removeMessages(HANDLER_COUNTER);
            activity.mLayoutCounter.setVisibility(TextView.GONE);
            mCamera.takePicture(null, null, activity.mPictureCallback);
          } else {
            int interval = COUNTER_INTERVAL;
            String counterText = activity.mCounter + "";
            if (activity.mCounter == COUNTER_COUNT) {
              counterText = "GO";
              interval = COUNTER_INTERVAL + 500;
            }

            activity.mTvCounter.setText(counterText);

            activity.mUIHandler.removeMessages(HANDLER_COUNTER);
            Message msgCounter = activity.mUIHandler
                .obtainMessage(HANDLER_COUNTER);
            activity.mUIHandler.sendMessageDelayed(msgCounter, interval);
          }
          break;
        }
      }
    }
  }

  @Override
  protected void onResume() {
    super.onResume();

    setCameraAndPreview();

    registerSensor();
  }

  @SuppressWarnings("deprecation")
  private void setCameraAndPreview() {
    if (checkCameraHardware()) {
      if (mCamera == null) {
        Log.d("camera", "get new instance");
        mCamera = getCameraInstance();
      } else {
        try {
          mCamera.reconnect();
        } catch (IOException e) {
        }
      }

      // Create our Preview view and set it as the content of our activity.
      if (mPreview == null) {
        mPreview = new CameraPreview(this, mCamera);
        // mPreview = new Preview(this);
        // mPreview.setCamera(mCamera);

        mCameraPreview = (FrameLayout) findViewById(R.id.camera_preview);
        mCameraPreview.addView(mPreview);

        mLayoutCameraFrame = (FrameLayout) findViewById(R.id.camera_frame);

        mLayoutCameraFrame.setBackgroundDrawable(getBackgroundDrawable());

        mLayoutCameraFrame.requestLayout();

        // Add a listener to the Capture button
        Button buttonTake = (Button) findViewById(R.id.buttonTake);

        mLayoutCounter = (FrameLayout) findViewById(R.id.layoutCounter);
        mLayoutCounter.setVisibility(TextView.GONE);
        mTvCounter = (TextView) findViewById(R.id.tvCounter);
        buttonTake.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            mCounter = 1;
            String counterText = mCounter + "";
            mTvCounter.setText(counterText);
            mLayoutCounter.setVisibility(TextView.VISIBLE);

            mUIHandler.removeMessages(HANDLER_COUNTER);
            Message msgCounter = mUIHandler.obtainMessage(HANDLER_COUNTER);
            mUIHandler.sendMessageDelayed(msgCounter, COUNTER_INTERVAL);
          }
        });

        Button buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            finish();
          }
        });

      }
    }

  }

  @Override
  protected void onPause() {
    super.onPause();
    if (mCamera != null) {
      mCamera.release();
      mCamera = null;
      mPreview = null;
      mCameraPreview.removeAllViews();
    }

    unregisterSensor();
  }

  private void unregisterSensor() {
    try {
      mSensorManager.unregisterListener(mSensorListener);
      mSensorListener = null;
    } catch (NullPointerException e) {
    }

  }

  public static final int MEDIA_TYPE_IMAGE = 1;
  public static final int MEDIA_TYPE_VIDEO = 2;
  private PictureCallback mPictureCallback = new PictureCallback() {

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

      File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE, "");
      if (pictureFile == null) {
        Log.d(TAG, "Error creating media file, check storage permissions: ");
        return;
      }

      byte[] pictureBytes = null;
      Bitmap bmOriginal = BitmapFactory.decodeByteArray(data, 0, data.length);
      Matrix m = new Matrix();

      boolean tablet = mApplication.isTablet();
      switch (mOrientation) {
      case ORIENTATION_LANDSCAPE_LEFT:
        if (tablet) {
          m.postRotate(90);
        } else {
          m.postRotate(270);
        }
        break;

      case ORIENTATION_LANDSCAPE_RIGHT:
        if (tablet) {
          m.postRotate(270);
        } else {
          m.postRotate(180);
        }
        break;

      case ORIENTATION_PORTRAIT_UP:
        if (tablet) {
          m.postRotate(0);
        } else {
          m.postRotate(270);
        }
        break;

      case ORIENTATION_PORTRAIT_DOWN:
        if (tablet) {
          m.postRotate(180);
        } else {
          m.postRotate(90);
        }

        break;
      }
      // mBackgroundFile

      bmOriginal = Bitmap.createBitmap(bmOriginal, 0, 0, bmOriginal.getWidth(),
          bmOriginal.getHeight(), m, true);
      bmOriginal = flip(bmOriginal, FLIP_HORIZONTAL);

      Log.d("bmOriginal",
          "w: " + bmOriginal.getWidth() + ", h: " + bmOriginal.getHeight());

      int beforeWidth = bmOriginal.getWidth();
      // int beforeHeight = bmOriginal.getHeight();
      int desireWidth = mBmFrame.getWidth();
      float ratio = ((float) beforeWidth / (float) desireWidth);
      int desireHeight = (int) Math.round(bmOriginal.getHeight() / ratio) + 50;

      int ystart = (mBmFrame.getHeight() - desireHeight) / 2;
      // int xstart = (bmOriginal.getWidth() - desireWidth) / 2;

      bmOriginal = Bitmap.createScaledBitmap(bmOriginal, desireWidth,
          desireHeight, true);

      Log.d("mBmFrame",
          "w: " + mBmFrame.getWidth() + ", h: " + mBmFrame.getHeight());
      Bitmap result = Bitmap.createBitmap(mBmFrame.getWidth(),
          mBmFrame.getHeight(), Config.ARGB_8888);

      // draw 2 images one over another one
      Canvas mCanvas = new Canvas(result);
      mCanvas.drawBitmap(bmOriginal, 0, ystart, null);
      mCanvas.drawBitmap(mBmFrame, 0, 0, null);

      // scale down result photo to 3 of 4
      result = Bitmap.createScaledBitmap(result, (desireWidth / 3),
          (desireHeight / 3), true);

      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      result.compress(CompressFormat.JPEG, 100, bos);
      pictureBytes = bos.toByteArray();
      try {
        FileOutputStream fos = new FileOutputStream(pictureFile);
        fos.write(pictureBytes);
        fos.close();
      } catch (FileNotFoundException e) {
        Log.d(TAG, "File not found: " + e.getMessage());
      } catch (IOException e) {
        Log.d(TAG, "Error accessing file: " + e.getMessage());
      }

      final String previewFileName = pictureFile.getAbsolutePath();

      MediaScannerWrapper scanner = new MediaScannerWrapper(getBaseContext(),
          pictureFile.getAbsolutePath(), "image/jpeg",
          new MediaScannerWrapper.OnScanMedia() {

            @Override
            public void onScanMediaComplete() {
              // close and go back
              mCamera.release();
              mCamera = null;
              mPreview = null;
              PhotoPreviewActivity.startForResult(LiveCameraActivity.this,
                  previewFileName);

            }
          });
      scanner.scan();
    }
  };

  // type definition
  public static final int FLIP_VERTICAL = 1;
  public static final int FLIP_HORIZONTAL = 2;

  public static Bitmap flip(Bitmap src, int type) {
    // create new matrix for transformation
    Matrix matrix = new Matrix();
    // if vertical
    if (type == FLIP_VERTICAL) {
      // y = y * -1
      matrix.preScale(1.0f, -1.0f);
    }
    // if horizonal
    else if (type == FLIP_HORIZONTAL) {
      // x = x * -1
      matrix.preScale(-1.0f, 1.0f);
      // unknown type
    } else {
      return null;
    }

    // return transformed image
    return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(),
        matrix, true);
  }

  public int mCamId;

  /** Check if this device has a camera */
  private boolean checkCameraHardware() {
    if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
      // this device has a camera
      return true;
    } else {
      // no camera on this device
      return false;
    }
  }

  /** Create a File for saving an image or video */
  @SuppressLint("SimpleDateFormat")
  private static File getOutputMediaFile(int type, String suff) {
    // To be safe, you should check that the SDCard is mounted
    // using Environment.getExternalStorageState() before doing this.

    File mediaStorageDir = new File(InstructionActivity.MUSEUM_FOLDER);
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.

    // Create the storage directory if it does not exist
    if (!mediaStorageDir.exists()) {
      if (!mediaStorageDir.mkdirs()) {
        Log.d("MyCameraApp", "failed to create directory");
        return null;
      }
    }

    // Create a media file name
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
        .format(new Date());
    File mediaFile;
    if (type == MEDIA_TYPE_IMAGE) {
      mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_"
          + timeStamp + suff + ".jpg");
    } else if (type == MEDIA_TYPE_VIDEO) {
      mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_"
          + timeStamp + suff + ".mp4");
    } else {
      return null;
    }

    return mediaFile;
  }

  /** A safe way to get an instance of the Camera object. */
  public Camera getCameraInstance() {
    Camera c = null;
    try {
      // releaseCameraAndPreview();
      int camId = Camera.CameraInfo.CAMERA_FACING_FRONT;
      if (Camera.getNumberOfCameras() > 1
          && camId < Camera.getNumberOfCameras() - 1) {
        mCamId = camId + 1;
        c = Camera.open(mCamId);
      } else {
        mCamId = camId;
        c = Camera.open(mCamId); // attempt to get a Camera instance
      }

    } catch (Exception e) {
      // Camera is not available (in use or does not exist)
    }
    return c; // returns null if camera is unavailable
  }

}
