package com.bomzaiya.museumpost;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.bomzaiya.data.GMailSender;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.view.WindowManager;

/**
 * This is just used in order to be able to recognize home button presses
 * 
 * @author Apisarn
 * 
 */
@SuppressLint("WorldReadableFiles")
public class SettingPreferenceActivity extends BomPrefActivity {
  public static String PREF_SCREEN_WAKEUP = "screen_wakeup";

  protected ProductConfig mProductConfig;
  protected ProgressDialog mProgressDialog = null;

  public static void start(BomActivity activity) {
    Intent intent = new Intent(activity.getBaseContext(), SettingPreferenceActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    activity.startActivity(intent);
  }

  public static void startForResult(BomActivity activity) {
    Intent intent = new Intent(activity.getBaseContext(), SettingPreferenceActivity.class);
    activity.startActivityForResult(intent, 1);
  }

  public void saveStat(String text) {
    File logFile = new File(PreloadActivity.FRAME_FOLDER, "stat.txt");

    String log = text;

    if (!logFile.exists()) {
      try {
        logFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    try {
      // BufferedWriter for performance, true to set append to file flag
      BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true), 8096);
      buf.append(log);
      buf.newLine();
      buf.flush();
      buf.close();
    } catch (IOException e) {
    }
  }

  @SuppressWarnings("deprecation")
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    addPreferencesFromResource(R.xml.settings);

    mApplication.setSharePreferenceBooleanValue("screen_wakeup", true);

    Preference buttonSendStat = (Preference) findPreference("buttonSendStat");
    buttonSendStat.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
      @Override
      public boolean onPreferenceClick(Preference arg0) {
        StatDatabaseAdapter db = new StatDatabaseAdapter(mApplication);
        Cursor cursor = db.getDateStats();
        if (cursor != null) {
          if (cursor.getCount() > 0) {
            String stats = "";
            String dateLabel = getString(R.string.stat_date);
            String likeLabel = getString(R.string.stat_like);
            String dislikeLabel = getString(R.string.stat_dislike);
            String frame1Label = getString(R.string.stat_frame1);
            String frame2Label = getString(R.string.stat_frame2);
            String frame3Label = getString(R.string.stat_frame3);
            String frame4Label = getString(R.string.stat_frame4);
            while (cursor.moveToNext()) {
              int id = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUMN_ID));
              int like = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUMN_LIKE));
              int dislike = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUMN_DISLIKE));
              int frame1 = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUMN_FRAME_ONE));
              int frame2 = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUMN_FRAME_TWO));
              int frame3 = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUMN_FRAME_THREE));
              int frame4 = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUMN_FRAME_FOUR));
              String stat_date = cursor.getString(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUMN_STAT_DATE));
              
              // stat detail
              stats += dateLabel + " " + stat_date + "\n";
              stats += likeLabel + " " + like + "\n";
              stats += dislikeLabel + " " + dislike + "\n";
              stats += frame1Label + " " + frame1 + "\n";
              stats += frame2Label + " " + frame2 + "\n";
              stats += frame3Label + " " + frame3 + "\n";
              stats += frame4Label + " " + frame4 + "\n";
              stats += "----------------------\n";
            }

            final String allstats = stats;
            Thread gmailSendThread = new Thread(new Runnable() {

              @Override
              public void run() {
                GMailSender sender = new GMailSender(ProductConfig.GMAIL_ACCOUNT, ProductConfig.GMAIL_PASSWORD);
                try {
                  sender.sendMail(getString(R.string.stat_email_subject) + " " + mApplication.getPhoneIMEI(), allstats,
                      ProductConfig.GMAIL_ACCOUNT, ProductConfig.STAT_RECEIPTS);
                } catch (Exception e) {
                  // e.printStackTrace();
                }
              }
            });

            gmailSendThread.start();

          }
        }
        db.close();
        return true;
      }
    });

    final CheckBoxPreference screen_wakeup = (CheckBoxPreference) findPreference("screen_wakeup");

    screen_wakeup.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

      @Override
      public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean checked = Boolean.parseBoolean(newValue.toString());

        mApplication.setSharePreferenceBooleanValue("screen_wakeup", checked);

        if (checked) {
          makeScreenOn();
        } else {
          clearScreenOn();
        }
        return true;
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();

    boolean screen_on = mApplication.getSharePreferenceBooleanValue("screen_wakeup");

    if (screen_on) {
      makeScreenOn();
    } else {
      clearScreenOn();
    }
  }

  private void makeScreenOn() {
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  private void clearScreenOn() {
    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  @Override
  public void onStop() {
    super.onStop();

  }

  @SuppressWarnings("deprecation")
  private void configurePreference() {
    getPreferenceManager().setSharedPreferencesName(BomApplication.PREFERENCES);
    getPreferenceManager().setSharedPreferencesMode(BomApplication.PREFERENCE_MODE);
  }

}
