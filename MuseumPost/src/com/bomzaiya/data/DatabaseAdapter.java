package com.bomzaiya.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class DatabaseAdapter {
  private Context context = null;
  private DatabaseHelper DBHelper;

  protected static final boolean FORCE_USE_FINGI = true;

  public static final boolean COPYDATABASE_FORCE_OVERWRITE = true;
  public static final boolean COPYDATABASE_NOT_OVERWRITE = false;

  protected static SQLiteDatabase db = null;

  public static String DATABASE_STATUS = "";

  protected DatabaseAdapter(Context ctx) {
    this.context = ctx;
    DBHelper = new DatabaseHelper(this.context);
  }

  protected DatabaseAdapter(Context ctx, String folder) {
    this.context = ctx;
    DBHelper = new DatabaseHelper(this.context, folder);
  }

  protected DatabaseAdapter(Context ctx, String folder, String dbname) {
    this.context = ctx;
    DBHelper = new DatabaseHelper(this.context, folder, dbname);
  }

  protected DatabaseAdapter(Context ctx, boolean forceUseFingi) {
    this.context = ctx;

    DBHelper = new DatabaseHelper(this.context);
  }

  public DatabaseHelper getDatabaseHelper() {
    return this.DBHelper;
  }

  public String toDbString(String text) {
    return "'" + text.trim() + "'";
  }

  public boolean isReady() {
    return this.DBHelper.isReady();
  }

  public void close() {
    DBHelper.close();
  }

  public static class DatabaseHelper extends SQLiteOpenHelper {
    // The Android's default system path of your application database.
    private String DB_PATH;

    private String DB_NAME = "bus.sqlite";
    private static final Integer DB_VERSION = 1;
    private final Context myContext;

    private final Integer BUFFER_SIZE = 512;

    private Boolean mCheckDatabase = false;

    /**
     * Constructor Takes and keeps a reference of the passed context in order to
     * access to the application assets and resources.
     * 
     * @param context
     */
    @SuppressLint("SdCardPath")
    public DatabaseHelper(Context context) {
      super(context, "xxx", null, DB_VERSION);

      this.myContext = context;
      DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";

      try {
        if (!mCheckDatabase) {
          this.checkDatabase();
          mCheckDatabase = true;
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    /**
     * Constructor Takes and keeps a reference of the passed context in order to
     * access to the application assets and resources.
     * 
     * @param context
     */
    @SuppressLint("SdCardPath")
    public DatabaseHelper(Context context, String dbName) {
      super(context, dbName, null, DB_VERSION);
      DB_NAME = dbName;

      this.myContext = context;
      DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";

      try {
        if (!mCheckDatabase) {
          this.checkDatabase();
          mCheckDatabase = true;
        }
      } catch (IOException e) {
      }
    }

    public boolean isReady() {
      return mCheckDatabase;
    }

    /**
     * Constructor Takes and keeps a reference of the passed context in order to
     * access to the application assets and resources.
     * 
     * @param context
     */
    public DatabaseHelper(Context context, String folder, String dbname) {
      super(context, dbname, null, DB_VERSION);
      DB_NAME = dbname;
      DB_PATH = folder + "/";
      this.myContext = context;

    }

    @Override
    public synchronized SQLiteDatabase getReadableDatabase() {
      // SystemHelper.appendLog("db_", DB_PATH + DB_NAME);
      db = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
          SQLiteDatabase.CREATE_IF_NECESSARY
              | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
      // SystemHelper.appendLog("db_", "open: " + db.isOpen());
      return db;
    }

    @Override
    public synchronized SQLiteDatabase getWritableDatabase() {
      // SystemHelper.appendLog("db_", DB_PATH + DB_NAME);
      db = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
          SQLiteDatabase.CREATE_IF_NECESSARY
              | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
      return db;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
      // Log.v("db", "opening");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /**
     * check db if not exist, copy from assets
     * 
     */
    public void checkDatabase() throws IOException {
      boolean bDbExist = isDBExist();
      if (bDbExist) {
        DATABASE_STATUS = "finished";
      } else {
        try {
          copyDatabase();
        } catch (IOException e) {
        }
      }
    }

    @SuppressWarnings("unused")
    private boolean deleteDatabase() {
      String outFileName = DB_PATH + DB_NAME;
      File targetFile = new File(outFileName);
      targetFile.delete();
      return true;
    }

    @SuppressWarnings("unused")
    private boolean isSizeDifferent() {
      // any error means need copy

      InputStream inputStream = null;
      long source_filesize = 0;
      try {
        inputStream = myContext.getAssets().open(DB_NAME);
        byte[] buffer = new byte[BUFFER_SIZE];
        int length = 0;
        while ((length = inputStream.read(buffer)) > 0) {
          source_filesize += length;
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      String outFileName = DB_PATH + DB_NAME;
      File targetFile = new File(outFileName);
      long target_filesize = targetFile.length();

      if (source_filesize != target_filesize) {
        return true;
      } else {
        return false;
      }
    }

    /**
     * copy database general method over internet
     * 
     */
    private void copyDatabase() throws IOException {
      // Open your local db as the input stream
      InputStream myInput = myContext.getAssets().open(DB_NAME);

      // Path to the just created empty db
      String outFileName = DB_PATH + DB_NAME;

      // Open the empty db as the output stream
      OutputStream myOutput = new FileOutputStream(outFileName);

      // transfer bytes from the inputfile to the outputfile
      byte[] buffer = new byte[BUFFER_SIZE];
      int length;
      while ((length = myInput.read(buffer)) > 0) {
        myOutput.write(buffer, 0, length);
      }
      DATABASE_STATUS = "finished";
      // Close the streams
      myOutput.flush();
      myOutput.close();
      myInput.close();
    }

    /**
     * copy database general method over internet
     * 
     */
    public void replaceDataBase(String filename, boolean forceOverride)
        throws IOException {
      // crate file descriptor
      File newTempDb = new File(filename);

      // get new db file name
      String newDbFileName = newTempDb.getName();

      // create input stream
      InputStream fileInputStream = new FileInputStream(newTempDb);

      String outFileName = DB_PATH + newDbFileName;

      File outputFile = new File(outFileName);

      if (!outputFile.exists() || forceOverride) {
        if (forceOverride) {
          outputFile.delete();
        }
        // Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[BUFFER_SIZE];
        int length;
        while ((length = fileInputStream.read(buffer)) > 0) {
          myOutput.write(buffer, 0, length);
        }
        DATABASE_STATUS = "finished";
        // Close the streams
        myOutput.flush();
        myOutput.close();
        fileInputStream.close();
      }
    }

    /**
     * existing of the db if db exists, will not copy
     */
    private boolean isDBExist() {
      // if databases folder not create, create!
      File dbFolder = new File(DB_PATH);
      if (!dbFolder.exists()) {
        dbFolder.mkdir();
        return false;
      }

      File dbFile = new File(DB_PATH + DB_NAME);
      if (!dbFile.exists()) {
        return false;
      }

      SQLiteDatabase checkDB = null;

      try {
        String myPath = DB_PATH + DB_NAME;
        // SQLiteDatabase.NO_LOCALIZED_COLLATORS;
        checkDB = SQLiteDatabase.openDatabase(myPath, null,
            SQLiteDatabase.OPEN_READONLY
                & SQLiteDatabase.NO_LOCALIZED_COLLATORS);

      } catch (SQLiteException e) {
        // database does't exist yet.
      }

      if (checkDB != null) {
        checkDB.close();
      }

      return checkDB != null ? true : false;
    }

    @Override
    public synchronized void close() {
      if (db != null) {
        db.close();
        super.close();
      }
    }

  }
}